package com.example.nychighschool.data

import android.net.Uri
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class HighSchool(

    @SerializedName("dbn") var dbn: String? = null,
    @SerializedName("school_name") var schoolName: String? = null,
    @SerializedName("overview_paragraph") var overview_paragraph: String? = null,
    @SerializedName("boro") var boro: String? = null,
    @SerializedName("location") var location: String? = null,
    @SerializedName("phone_number") var phoneNumber: String? = null,
    @SerializedName("fax_number") var faxNumber: String? = null,
    @SerializedName("school_email") var schoolEmail: String? = null,

    ) {
    override fun toString(): String = Uri.encode(Gson().toJson(this))
}
