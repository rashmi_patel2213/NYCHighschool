package com.example.nychighschool.Repository

// HighSchoolsRepository.kt

import com.example.nychighschool.data.HighSchool
import com.example.nychighschool.Api.HighSchoolsService
import com.example.nychighschool.data.SATscore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class HighSchoolsRepository {

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(
            OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build()
        )
        .build()

    private val service = retrofit.create(HighSchoolsService::class.java)

    suspend fun getHighSchools(): List<HighSchool> {
        return withContext(Dispatchers.IO) {
            service.getHighSchools()
        }
    }

    suspend fun fetchHighSchoolSatScore(): List<SATscore> {
        return service.getHighSchoolSatScore()
    }
}


