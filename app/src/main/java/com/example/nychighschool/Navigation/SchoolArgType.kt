package com.example.nychighschool.Navigation

import com.example.nychighschool.data.HighSchool
import com.google.gson.Gson

class SchoolArgType : JsonNavType<HighSchool>() {
    override fun fromJsonParse(value: String): HighSchool = Gson().fromJson(value, HighSchool::class.java)

    override fun HighSchool.getJsonParse(): String = Gson().toJson(this)
}