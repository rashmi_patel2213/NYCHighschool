package com.example.nychighschool.Navigation
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.navigation.compose.composable
import androidx.navigation.compose.NavHost
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.nychighschool.compose.Screens.Screen
import com.example.nychighschool.Screens.HighSchoolDetailsScreen
import com.example.nychighschool.Screens.HighSchoolListScreen
import com.example.nychighschool.ViewModel.HighSchoolsViewModel
import com.example.nychighschool.data.HighSchool
import com.google.gson.Gson

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun NYCShoolApp(viewModel: HighSchoolsViewModel) {
    val navController = rememberNavController()
    NYschoolNavHost(
        navController = navController,
        viewModel = viewModel
    )
}


@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun NYschoolNavHost(
    navController: NavHostController,
    viewModel: HighSchoolsViewModel
) {

    NavHost(navController = navController, startDestination = Screen.HighSchoolList.route) {
        composable(route = Screen.HighSchoolList.route) {
            HighSchoolListScreen(
                viewModel = viewModel,
                onItemClick = {
                    navController.navigate(
                        Screen.HighSchoolDetails.createRoute(
                            highschool = it.toString()
                        )
                    )
                }
            )
        }
        composable(
            route = Screen.HighSchoolDetails.route,
            arguments = Screen.HighSchoolDetails.navArguments
        ) { backStackEntry ->
            val highSchool: HighSchool =
                backStackEntry.arguments?.getString("schoolDbn") ?. let { Gson().fromJson(it, HighSchool::class.java) }!!

            HighSchoolDetailsScreen(
                viewModel,
                highSchool

            )
        }
    }
}
