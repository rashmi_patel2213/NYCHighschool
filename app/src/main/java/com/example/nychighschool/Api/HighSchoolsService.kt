package com.example.nychighschool.Api

// HighSchoolsService.kt

import com.example.nychighschool.data.HighSchool
import com.example.nychighschool.data.SATscore
import retrofit2.http.GET

interface HighSchoolsService {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getHighSchools(): List<HighSchool>
    @GET("resource/f9bf-2cp4.json")
    suspend fun getHighSchoolSatScore(): List<SATscore>
}
