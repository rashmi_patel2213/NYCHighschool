package com.example.nychighschool.activity

import android.os.Build
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.nychighschool.ViewModel.HighSchoolsViewModel
import com.example.nychighschool.Navigation.NYCShoolApp
import com.example.nychighschool.ui.theme.NYCHighschoolTheme


class MainActivity : AppCompatActivity() {
    private val viewModel: HighSchoolsViewModel by viewModels()
    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
    NYCHighschoolTheme {
        NYCShoolApp(viewModel = viewModel)
                }
        }
    }
}
