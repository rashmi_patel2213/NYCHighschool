package com.example.nychighschool.Screens


import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.nychighschool.data.HighSchool
import com.example.nychighschool.ViewModel.HighSchoolsViewModel
import com.example.nychighschool.data.SATscore

@Composable
fun HighSchoolDetailsScreen(viewModel: HighSchoolsViewModel, highSchool: HighSchool) {
    val highSchoolsatscore: List<SATscore> by viewModel.highSchoolsatscore.observeAsState(emptyList())

    val matchingSATscores = highSchoolsatscore.filter { it.dbn == highSchool.dbn }
    Log.i("matching",""+matchingSATscores)

   LaunchedEffect(key1 = Unit) {
       viewModel.fetchHighSchoolSatScore()
   }

    Column(
        modifier = Modifier
            .padding(16.dp)
            .verticalScroll(rememberScrollState())
    ) {
        Text(text = "School Name: ${highSchool.schoolName}", fontWeight = FontWeight.Bold)
        Spacer(modifier = Modifier.height(8.dp))

        Text(text = "Overview: ${highSchool.overview_paragraph}")
        Spacer(modifier = Modifier.height(8.dp))

        Text(text = "Location: ${highSchool.location}")
        Spacer(modifier = Modifier.height(8.dp))

        Text(text = "Phone Number: ${highSchool.phoneNumber}")
        Spacer(modifier = Modifier.height(16.dp))

        matchingSATscores.forEach { satScore ->
            // Display the Math average score for each SAT score
            Text(text = "SAT Math Average Score: ${satScore.satMathAvgScore}")
            Spacer(modifier = Modifier.height(8.dp))

            Text(text = "SAT writing Average Score: ${satScore.satWritingAvgScore}")
            Spacer(modifier = Modifier.height(8.dp))

            Text(text = "SAT critical Reading Avg Score: ${satScore.satCriticalReadingAvgScore}")
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}