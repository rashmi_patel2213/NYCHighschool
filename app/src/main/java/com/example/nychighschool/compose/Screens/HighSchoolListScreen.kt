package com.example.nychighschool.Screens

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.nychighschool.data.HighSchool
import com.example.nychighschool.ViewModel.HighSchoolsViewModel

@SuppressLint("SuspiciousIndentation")
@Composable
fun HighSchoolListScreen(
    viewModel: HighSchoolsViewModel,
    onItemClick: (HighSchool) -> Unit = {},
) {
    val highSchools: List<HighSchool> by viewModel.highSchools.observeAsState(emptyList())

        // Launch the effect to fetch high schools when the screen is initially composed
        LaunchedEffect(Unit) {
            viewModel.fetchHighSchools()
         }

    LazyColumn(
        modifier = Modifier.padding(16.dp)
    ) {
        items(highSchools) { highSchool ->
            HighSchoolListItem(highSchool, onItemClick)
        }
    }
}

@Composable
fun HighSchoolListItem(
    highSchool: HighSchool,
    onItemClick: (HighSchool) -> Unit

) {    Text(
        text = highSchool.schoolName ?:"",
        modifier = Modifier
            .padding(16.dp)
            .clickable { onItemClick(highSchool) }
    )
}

@Composable
fun InternetConnectivityAlert(
    context: Context,
    connectivityManager: ConnectivityManager,
    onConnectivityAvailable: () -> Unit,
    onConnectivityUnavailable: () -> Unit
) {
    val networkInfo = connectivityManager.activeNetworkInfo
    val isConnected = networkInfo?.isConnectedOrConnecting == true

    if (isConnected) {
        onConnectivityAvailable()
    } else {
        onConnectivityUnavailable()
    }
}